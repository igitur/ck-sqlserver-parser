﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.SqlServer.Parser
{
    /// <summary>
    /// A Transformation statement is considered as a statement.
    /// </summary>
    public interface ISqlTStatement : ISqlStatement
    {
    }
}
